package com.example.jarlaslnovo15.livrinho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    Button btnpassa;
    public EditText nome1;
    public EditText nome2;
    ImageView fundo;
    public String editValor1;
    public String editValor2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnpassa = (Button)findViewById(R.id.button5);
        nome1 = (EditText)findViewById(R.id.editText4);
        nome2 = (EditText)findViewById(R.id.editText3);
        fundo = (ImageView)findViewById(R.id.imageView3);
    }
    public void btnpassa(View v){
        Intent i = new Intent(this, Main2Activity.class);

        editValor1 = nome1.getText().toString();
        i.putExtra("Value", editValor1);

        editValor2 = nome2.getText().toString();
        i.putExtra("Value", editValor2);


        startActivity(i);
        finish();

    }
}
