package com.example.jarlaslnovo15.livrinho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView textoNome1;
    ImageView fundo;
    Button btnpassa2;
    String valorRecebido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textoNome1 = (TextView)findViewById(R.id.textView);
        fundo = (ImageView)findViewById(R.id.imageView);
        btnpassa2 = (Button)findViewById(R.id.button3);

        valorRecebido = getIntent().getExtras().getString("Value");
        textoNome1.setText(valorRecebido);
    }
    public void btnpassa2(View v){
        Intent i = new Intent(this, Main3Activity.class);
        startActivity(i);
        finish();

    }
}
