package com.example.jarlaslnovo15.livrinho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Main4Activity extends AppCompatActivity {
    ImageView fundo;
    Button btnpassa4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        fundo = (ImageView)findViewById(R.id.imageView4);
        btnpassa4 = (Button)findViewById(R.id.button);
    }
    public void btnpassa4(View v){
        Intent i = new Intent(this, Main5Activity.class);
        startActivity(i);
        finish();
    }
}
